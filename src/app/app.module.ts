import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import{AngularFireModule} from 'angularfire2';

import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { PostsService } from './posts/posts.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';



export const firebaseConfig = {
     apiKey: "AIzaSyBq2I7wjT1jD9ECdRH3tAeBYktv74hUf6o",
    authDomain: "jion7-e995a.firebaseapp.com",
    databaseURL: "https://jion7-e995a.firebaseio.com",
    projectId: "jion7-e995a",
    storageBucket: "jion7-e995a.appspot.com",
    messagingSenderId: "910073041266"
}


const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: UsersComponent },
  { path: '', component: PostsComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    
    UserComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
     AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,PostsService],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
