import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;

  currentPost;
  isLoading = true;

  

    constructor(private _postsService: PostsService) {

      

   // this.posts = this._postsService.getPosts();
  }
   deletePost(post){
    this._postsService.deletePost(post);
  }
    updatePost(post){
    this._postsService.updatePost(post);
  }
  addPost(post){
    this._postsService.addPost(post);
  }

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData => 
    {this.posts = postsData;
      this.isLoading = false,
      console.log(this.posts)});
  }

}